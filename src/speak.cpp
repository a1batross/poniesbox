#include "configwindow.h"
#include "speak.h"
#include "pony.h"

// These are the variable types for Behavior configuration
const CSVParser::ParseTypes Speak::OptionTypes {
    {                     "type", QVariant::Type::String },
    {                     "name", QVariant::Type::String },
    {                     "text", QVariant::Type::String },
    {                  "{files}", QVariant::Type::String },
    {            "skip_normally", QVariant::Type::Bool }
};

//TODO: move player to Pony and pass a pointer on play() to it
Speak::Speak(Pony* parent, const QString filepath, const std::vector<QVariant> &options)
    :QObject(parent), parent(parent), path(filepath), audioOutput(nullptr), mediaObject(nullptr)
{

    if(options.size() == 2) { // Speak, "text"
        text = options[1].toString();
        skip_normally = false;
    }else{ // Speak, name, "text"
        name = options[1].toString().toLower();
        text = options[2].toString();

        if(options.size()>3){ // Speak, name, "text", {"file.mp3", "file.ogg"}, skip_normally
            if(options[3] != "") {
                soundfiles = options[3].toList();
            }

            skip_normally = options[4].toBool();
        }
    }
}

Speak::~Speak()
{
#ifdef USE_PHONON
    if(mediaObject != nullptr) {
        mediaObject->stop();
        delete mediaObject;
    }

    if(audioOutput != nullptr) delete audioOutput;
#endif
}

void Speak::play()
{
#ifdef USE_PHONON
    if(soundfiles.size() == 0) return;

    if(audioOutput == nullptr) {
        audioOutput = new Phonon::AudioOutput(Phonon::MusicCategory, this);
    }
    if(mediaObject == nullptr) {
        mediaObject = new Phonon::MediaObject(this);
    }

    mediaObject->setCurrentSource(ConfigWindow::conf_read("main","pony-directory").toString() + "/" + path + "/" + soundfiles[0].toString());
    connect(mediaObject, SIGNAL(finished()), this, SLOT(stop()));

    Phonon::createPath(mediaObject, audioOutput);

    mediaObject->play();
#endif
}

void Speak::stop()
{
#ifdef USE_PHONON
    if(mediaObject != nullptr) {
        mediaObject->stop();
        delete mediaObject;
        mediaObject = nullptr;
    }
    if(audioOutput != nullptr) {
        delete audioOutput;
        mediaObject = nullptr;
    }
#endif
}

#include <QApplication>
#include <QtCore>
#include <QtGui>

#include "singleapplication.h"
#include "csv_parser.h"
#include "behavior.h"
#include "effect.h"
#include "speak.h"

#include "configwindow.h"
#include "pony.h"

int main(int argc, char *argv[])
{
    CSVParser::AddParseTypes("Behavior", Behavior::OptionTypes);
    CSVParser::AddParseTypes("Effect", Effect::OptionTypes);
    CSVParser::AddParseTypes("Speak", Speak::OptionTypes);

    DSingleApplication app(NAME, argc, argv);
    QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());
    QCoreApplication::setOrganizationName(NAME);
    QCoreApplication::setApplicationName(NAME);

    QString locale = QLocale::system().name().left(2).toLatin1();
    QTranslator translator;

    if(!translator.load(QString("%1_%2").arg(NAME, locale), QApplication::applicationDirPath() + "/translations"))
        translator.load(QString("%1_%2").arg(NAME, locale), QString("%1/translations").arg(DATA_PATH));
    app.installTranslator(&translator);

    app.setQuitOnLastWindowClosed(false);
#ifdef IS_QT4
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));;
#endif
    QSettings::setDefaultFormat(QSettings::IniFormat);

    if ( app.isRunning() ) {
        app.sendMessage("showConfigWindow");
        return 0;
    }
    qDebug() << "Locale:" << locale;

    ConfigWindow config;
    QObject::connect(qApp, SIGNAL(messageReceived(QString)), &config, SLOT(receiveFromInstance(QString)));

    if(config.ponies.size() == 0) {
        config.show();
    }

    return app.exec();
}

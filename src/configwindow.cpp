#include "configwindow.h"
#include "ui_configwindow.h"

#ifdef Q_WS_X11
#include <QX11Info>
#include <X11/Xatom.h>
#include <X11/Xlib.h> // Xlib #defines None as 0L, which conflicts with Behavior::Movement::None
                      // This is why we include it after pony.h
#endif

// TODO: configuration:
//       do not stop on mouseover or avoidance zones (for the mouse cursor at first)
//       monitors (on witch to run, etc)
//
//	avoidance areas (vincity of the mouse cursor for example)
//FIXME: speech enabled toggle toggles sound group visibility

const std::unordered_map<QString, const QVariant> ConfigWindow::config_defaults {
    {"main/always-on-top",           true                                   },
    {"main/bypass-wm",               false                                  },
    {"main/pony-directory",          QString("%1/desktop-ponies").arg(DATA_PATH) },
    {"main/interactions-enabled",    true                                   },
    {"main/effects-enabled",         true                                   },
    {"main/small-ponies",            false                                  },
    {"main/hide-tray-icon",          false                                  },
    {"speech/enabled",               true                                   },
    {"speech/probability",           50                                     },
    {"speech/duration",              2000                                   },
    {"sound/enabled",                false                                  }
};

ConfigWindow::ConfigWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ConfigWindow)
{
    signal_mapper = new QSignalMapper();

    ui->setupUi(this);
    move_center();

    if( qgetenv("XAUTHORITY").isEmpty() ) {
        // Do not show X11 specific options without X11
        ui->label_bypass_wm->setVisible(false);
        ui->x11_bypass_wm->setVisible(false);
    }

#ifdef Q_WS_X11
    // Check what X11 window manager we are running (to fix wm quirks)
    detect_x11_wm();
#endif

    list_model = new QStandardItemModel(this);
    active_list_model = new QStandardItemModel(this);

    // Setup tray icon and menu
    tray_icon.setIcon(QIcon::fromTheme(NAME, QIcon(QString(":/res/icons/22x22/apps/%1.png").arg(NAME))));

    tray_menu.addAction(trUtf8("Open configuration"),this,SLOT(show()));
    tray_menu.addAction(trUtf8("Close application"),QCoreApplication::instance(),SLOT(quit()));

    connect(&tray_icon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(toggle_window(QSystemTrayIcon::ActivationReason)));
    tray_icon.setContextMenu(&tray_menu);

    load_settings();

    ui->tabbar->setShape(QTabBar::RoundedWest);

    // Load available ponies into the list
    reload_available_ponies();

    connect(ui->tabbar, SIGNAL(currentChanged(int)), this, SLOT(lettertab_changed(int)));

    ui->available_list->setIconSize(QSize(100,100));
    ui->available_list->setModel(list_model);
    ui->available_list->setAlternatingRowColors(true);

    ui->active_list->setIconSize(QSize(100,100));
    ui->active_list->setModel(active_list_model);
    ui->active_list->setAlternatingRowColors(true);

    connect(ui->available_list->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), this, SLOT(newpony_list_changed(QModelIndex)));

    // Start update timer
    update_timer.setInterval(30);
    update_timer.start();

    interaction_timer.setInterval(500);
    interaction_timer.start();

    QObject::connect(&interaction_timer, SIGNAL(timeout()), this, SLOT(update_interactions()));

    // Load every pony specified in configuration
    QSettings *settings = new QSettings();
    int size = settings->beginReadArray("loaded-ponies");
    for(int i=0; i< size; i++) {
        settings->setArrayIndex(i);
        try {
            ponies.emplace_back(std::make_shared<Pony>(settings->value("name").toString(), this));
            QObject::connect(&update_timer, SIGNAL(timeout()), ponies.back().get(), SLOT(update()));
        }catch (std::exception &e) {
            qCritical() << "Could not load pony" << settings->value("name").toString();
        }
    }
    settings->endArray();
    list_model->sort(1);

    update_active_list();

    // Load interactions
    QFile ifile(QString("%1/interactions.ini").arg(conf_read("main","pony-directory", settings).toString()));
    delete(settings);
    if(!ifile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qCritical() << "Cannot open interactions.ini";
        qCritical() << ifile.errorString();
    }

    if( ifile.isOpen() ) {
        QString line;
        QTextStream istr(&ifile);

        while (!istr.atEnd() ) {
            line = istr.readLine();

            if(line[0] != '\'' && !line.isEmpty()) {
                std::vector<QVariant> csv_data;
                CSVParser::ParseLine(csv_data, line, ',', Interaction::OptionTypes);
                try {
                    interactions.emplace_back(csv_data);
                }catch (std::exception &e) {
                    qCritical() << "Could not load interaction.";
                }

            }
        }

        ifile.close();
    }else{
        qCritical() << "Cannot read interactions.ini";
    }

}

ConfigWindow::~ConfigWindow()
{
    delete ui;
    delete signal_mapper;
    delete list_model;
}

void ConfigWindow::move_center() {
//var
    QRect rect = frameGeometry();
//endvar
    rect.moveCenter(QDesktopWidget().availableGeometry().center());
    move(rect.topLeft());
}

#ifdef Q_WS_X11
// Check under what window manager we are running. We use this information to fix the window manager quirks.
//
// eg: xprop -root | grep _NET_SUPPORTING_WM_CHECK # gives WINID
//     xprop -id $WINID  | grep _NET_WM_NAME # gives WM name, ie. "KWin"

void ConfigWindow::detect_x11_wm()
{
    Atom atom_supporting_wm = XInternAtom(QX11Info::display(), "_NET_SUPPORTING_WM_CHECK", False);
    Atom atom_wm_name = XInternAtom(QX11Info::display(), "_NET_WM_NAME", False);
    Atom atom_utf8_string = XInternAtom(QX11Info::display(), "UTF8_STRING", False);

    Atom type;
    int format;
    unsigned long num_items;
    unsigned long bytes_after;
    unsigned char *supporting_winid = nullptr;
    unsigned char *wm_name = nullptr;

    x11_wm = X11_WM_Types::Unknown;

    if(XGetWindowProperty(QX11Info::display(), QX11Info::appRootWindow() , atom_supporting_wm, 0, 1,
                          False, XA_WINDOW, &type, &format, &num_items, &bytes_after, &supporting_winid)
       == Success && supporting_winid != nullptr) {
        if (type == XA_WINDOW && format == 32) {
            if(XGetWindowProperty(QX11Info::display(), *(reinterpret_cast<Window *>(supporting_winid)), atom_wm_name, 0, 4,
                                  False, atom_utf8_string, &type, &format, &num_items, &bytes_after, &wm_name)
               == Success && wm_name != nullptr) {

                if (type == atom_utf8_string && format == 8) {
                    // Find out what WM is running
                    if(      !strcmp(reinterpret_cast<char *>(wm_name), "KWin")){
                        x11_wm = X11_WM_Types::KWin;
                    }else if(!strcmp(reinterpret_cast<char *>(wm_name), "compiz")){
                        x11_wm = X11_WM_Types::Compiz;
                    }
                }

                XFree(wm_name);
            }
        }

        XFree(supporting_winid);
    }
}

ConfigWindow::X11_WM_Types ConfigWindow::getX11_WM()
{
    return x11_wm;
}
#endif

QVariant ConfigWindow::conf_read(const QString &section, const QString &value, QSettings* settings) {
    bool isAllocated = false;
    if ( ! settings ) {
        settings = new QSettings();
        isAllocated = true;
    }
    settings->beginGroup(section);
    QVariant result = settings->value(value, "").toString();
    if ( result.toString().isEmpty() )
        result = config_defaults.at(section + "/" + value);
    settings->endGroup();
    if (isAllocated)
        delete(settings);
    return result;
}

void ConfigWindow::conf_write(const QString &section, const QString &value, const QVariant &inValue, QSettings* settings) {
    bool isAllocated = false;
    if ( ! settings ) {
        settings = new QSettings();
        isAllocated = true;
    }
    settings->beginGroup(section);
    settings->setValue(value, inValue);
    settings->endGroup();
    if (isAllocated)
        delete(settings);
}

void ConfigWindow::remove_pony()
{
    // Get a pointer to Pony from sender()
    QAction *q = qobject_cast<QAction*>(QObject::sender());
    Pony* p = static_cast<Pony*>(q->parent()->parent()); // QAction->QMenu->QMainWindow(Pony)
    ponies.remove(p->get_shared_ptr());

    save_settings();
    update_active_list();
}

void ConfigWindow::remove_pony_all()
{
    // Get a pointer to Pony from sender()
    QAction *q = qobject_cast<QAction*>(QObject::sender());
    Pony* p = static_cast<Pony*>(q->parent()->parent()); // QAction->QMenu->QMainWindow(Pony)
    QString pony_name(p->name); // We must copy the name, because it will be deleted
    ponies.remove_if([&pony_name](const std::shared_ptr<Pony> &pony){
        return pony->name == pony_name;
    });

    save_settings();
    update_active_list();
}

void ConfigWindow::remove_pony_activelist()
{

    // For each of the selected items
    for(auto &i: ui->active_list->selectionModel()->selectedIndexes() ) {
        if(i.column() == 0) {
            // Ignore the first column(with the icon), we are only interested in the second column (with the name of the pony)
            continue;
        }

        // Get the name from active list
        QString name = i.data().toString();

        // Find first occurance of pony name
        auto occurance = std::find_if(ponies.begin(), ponies.end(),
                                         [&name](const std::shared_ptr<Pony> &p)
                                         {
                                             return p->directory == name;
                                         });
        // If found, remove
        if(occurance != ponies.end()) {
            ponies.erase(occurance);
        }

    }

    save_settings();
    update_active_list();
}

void ConfigWindow::reload_available_ponies()
{
    list_model->clear();
    int count = ui->tabbar->count();
    for(int i = 0; i < count; i++) {
        ui->tabbar->removeTab(0);
    }

    QDir dir(conf_read("main","pony-directory").toString());
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    // Get names of all the pony directories
    QList<QChar> letters;
    for(auto &i: dir.entryList()) {
        QDir pony_dir(dir);
        pony_dir.cd(i);
        if(pony_dir.exists("pony.ini")) {
            // Get the letters for TabBar for quick navigation of the available pony list
            if(!letters.contains(i[0])) {
                // Add the first letter of the name if we do not have it already
                letters.push_back(i[0]);
            }

            QStandardItem *item_icon = new QStandardItem(QIcon(pony_dir.absoluteFilePath("icon.png")),"");
            QStandardItem *item_text = new QStandardItem(i);

            QList<QStandardItem*> row;
            row << item_icon << item_text;
            list_model->appendRow(row);
        }
    }
    for(QChar &i: letters) ui->tabbar->addTab(i);


}

void ConfigWindow::newpony_list_changed(QModelIndex item)
{
    // Update the UI with information about selected pony
    ui->image_label->setPixmap(item.sibling(item.row(),0).data(Qt::DecorationRole).value<QIcon>().pixmap(100,100));
    ui->label_ponyname->setText(item.sibling(item.row(),1).data().toString());
}

void ConfigWindow::add_pony()
{
    // For each of the selected items
    for(auto &i: ui->available_list->selectionModel()->selectedIndexes() ) {
        if(i.column() == 0) {
            // Ignore the first column(with the icon), we are only interested in the second column (with the name of the pony)
            continue;
        }

        // Get the name from active list
        QString name = i.data().toString();

        try {
            // Try to initialize the new pony at the end of the active pony list and connect it to the update timer
            ponies.emplace_back(std::make_shared<Pony>(i.data().toString(), this));
            QObject::connect(&update_timer, SIGNAL(timeout()), ponies.back().get(), SLOT(update()));

        }catch (std::exception &e) {
            qCritical() << "Could not load pony" << name;
        }

    }

    save_settings();
    update_active_list();
}

void ConfigWindow::update_active_list()
{
    active_list_model->clear();
    for(auto &i: ponies) {
        QStandardItem *item_icon = new QStandardItem(QIcon(QString("%1/%2/icon.png").arg(ConfigWindow::conf_read("main","pony-directory").toString(), i->directory)),"");
        QStandardItem *item_text = new QStandardItem(i->directory);

        QList<QStandardItem*> row;
        row << item_icon << item_text;
        active_list_model->appendRow(row);
    }
    active_list_model->sort(1);

    if ( conf_read("main","hide-tray-icon").toBool() & (active_list_model->rowCount() == 0) )
        show();
}

void ConfigWindow::lettertab_changed(int index)
{
    // Find items starting with the letter which is currently selected in the tab bar
    QList<QStandardItem *> found = list_model->findItems(ui->tabbar->tabText(index),Qt::MatchStartsWith, 1);
    if(!found.isEmpty()) { // It should always find something
        // Scroll active list to the first found item
        ui->available_list->scrollTo(found[0]->index(), QAbstractItemView::PositionAtTop);
    }
}

void ConfigWindow::toggle_window(QSystemTrayIcon::ActivationReason reason)
{
    // Toogle the configuration window's visibility
    if(reason == QSystemTrayIcon::DoubleClick) {
        if(this->isVisible() == true)
        {
            hide();
        }else{
            show();
        }
    }
}

void ConfigWindow::change_ponydata_directory()
{
    QString new_dir = QFileDialog::getExistingDirectory(this, trUtf8("Select pony data directory"), conf_read("main","pony-directory").toString(),
                                                        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );

    if(new_dir != "") {
        ui->ponydata_directory->setText(new_dir);
    }
}

void ConfigWindow::load_settings()
{
    QSettings settings;

    // Main settings
    ui->alwaysontop->setChecked(         conf_read    ("main","always-on-top", &settings).toBool());
    ui->x11_bypass_wm->setChecked(       conf_read    ("main","bypass-wm", &settings).toBool());
    ui->ponydata_directory->setText(     conf_read    ("main","pony-directory", &settings).toString());
    ui->interactions_enabled->setChecked(conf_read    ("main","interactions-enabled", &settings).toBool());
    ui->effects_enabled->setChecked(     conf_read    ("main","effects-enabled", &settings).toBool());
    ui->small_ponies->setChecked(        conf_read    ("main","small-ponies", &settings).toBool());
    ui->hide_tray_icon->setChecked(      conf_read    ("main","hide-tray-icon", &settings).toBool());

    // Speech settings
    ui->speechenabled->setChecked(  conf_read    ("speech","enabled", &settings).toBool());
    ui->textdelay->setValue(        conf_read    ("speech","duration", &settings).toInt());
    ui->speechprobability->setValue(conf_read    ("speech","probability", &settings).toInt());

    // Sound settings
    ui->playsounds->setChecked(     conf_read    ("sound","enabled", &settings).toBool());

    // We do not load ponies here because we might use this function
    // to discard user made changes if user did not apply them

    if (conf_read("main","hide-tray-icon").toBool())
        tray_icon.hide();
    else
        tray_icon.show();
}

void ConfigWindow::save_settings()
{
    QSettings settings;

    // Check if we have to update the pony windows with new always-on-top/bypass-wm value
    bool change_ontop = (conf_read("main","always-on-top", &settings).toBool() != ui->alwaysontop->isChecked());
    bool change_bypass_wm = (conf_read("main","bypass-wm", &settings).toBool() != ui->x11_bypass_wm->isChecked());
    bool reload_ponies = (conf_read("main","pony-directory", &settings).toString() != ui->ponydata_directory->text());

    // Write the program settings
    settings.clear();

    // Main settings
    conf_write("main","always-on-top", ui->alwaysontop->isChecked(), &settings);
    conf_write("main","bypass-wm", ui->x11_bypass_wm->isChecked(), &settings);
    conf_write("main","pony-directory", ui->ponydata_directory->text(), &settings);
    conf_write("main","interactions-enabled", ui->interactions_enabled->isChecked(), &settings);
    conf_write("main","effects-enabled", ui->effects_enabled->isChecked(), &settings);
    conf_write("main","small-ponies", ui->small_ponies->isChecked(), &settings);
    conf_write("main","hide-tray-icon", ui->hide_tray_icon->isChecked(), &settings);

    // Speech settings
    conf_write("speech","enabled", ui->speechenabled->isChecked(), &settings);
    conf_write("speech","duration", ui->textdelay->value(), &settings);
    conf_write("speech","probability", ui->speechprobability->value(), &settings);

    // Sound settings
    conf_write("sound","enabled", ui->playsounds->isChecked(), &settings);

    // Write the active ponies list
    settings.beginWriteArray("loaded-ponies");
    int i=0;
    for(const auto &pony : ponies) {
        if(change_ontop) {
            pony->set_on_top(ui->alwaysontop->isChecked());
        }
        if(change_bypass_wm) {
            pony->set_bypass_wm(ui->x11_bypass_wm->isChecked());
        }
        settings.setArrayIndex(i);
        settings.setValue("name", pony->directory);
        i++;
    }
    settings.endArray();

    if(reload_ponies) {
        reload_available_ponies();
    }

    if (conf_read("main","hide-tray-icon").toBool())
        tray_icon.hide();
    else
        tray_icon.show();

    // Make sure we write our changes to disk
    settings.sync();
}

void ConfigWindow::update_distances()
{
    distances.clear();
    for(const std::shared_ptr<Pony> &p1: ponies) {
        QPoint c1(p1->x_pos + p1->current_behavior->x_center, p1->y_pos + p1->current_behavior->y_center);
        for(const std::shared_ptr<Pony> &p2: ponies) {
            if(p1 == p2) continue;

            QPoint c2(p2->x_pos + p2->current_behavior->x_center, p2->y_pos + p2->current_behavior->y_center);

            float dist = std::sqrt(std::pow(c2.x() - c1.x(), 2) + std::pow(c2.y() - c1.y(), 2));

            distances.insert({{p1->name.toLower(), p2->name.toLower()}, dist});
        }
    }
}

void ConfigWindow::update_interactions()
{
    if(!conf_read("main","interactions-enabled").toBool()) return;

    update_distances();

    std::mt19937 gen(QDateTime::currentMSecsSinceEpoch());
    std::uniform_real_distribution<> real_dis(0, 1);


    // For each interaction
    for(auto &i: interactions){
        // check probability

        // For each pony that starts this interaction
        for(auto &p: ponies) {
            if(p->name.toLower() != i.pony) continue;
            if(p->in_interaction) continue;
            if((p->interaction_delays.find(i.name) != p->interaction_delays.end()) && // Check if there is an active delay for this interaction in this pony
                    (p->interaction_delays.at(i.name) > QDateTime::currentMSecsSinceEpoch())) continue;

            // TODO: add it to interaction instance, and when cancelling, cancel interaction for every pony in interaction
            std::vector<std::shared_ptr<Pony>> interaction_targets;

            // For each target of that interaction
            for(const QVariant &p_target: i.targets) {

                // For each found target
                for(auto &pp: ponies) {
                    if(pp == p) continue; // Do not interact with self
                    if(pp->name.toLower() != p_target.toString()) continue;

                    if(pp->in_interaction){
                        continue; // The pony is already in an interaction
                    }else if(pp->sleeping){
                        continue; // Sleeping ponies do not interact
                    }else if(distances.at(std::make_pair(p->name.toLower(), p_target.toString())) > i.distance){
                            continue; // The pony is too far, check the rest
                    }else if((pp->interaction_delays.find(i.name) != pp->interaction_delays.end()) && // Check if there is an active delay for this interaction in this pony
                                         (pp->interaction_delays.at(i.name) > QDateTime::currentMSecsSinceEpoch())) {
                        continue; // The pony has an active delay for this interaction
                    }else{
                        // We found a suitable pony, we can do the interaction
                        if(real_dis(gen) <= i.probability) {
                            // Only interact with specified probability
                            interaction_targets.push_back(pp);
                        }
                    }
                }
            }

            if(interaction_targets.empty()) {
                // We didn't find anypony to interact with, check the next initiating pony for this interaction
                continue;
            }

            QString selected_behavior = i.select_behavior();

            p->current_interaction = i.name;
            p->current_interaction_delay = i.reactivation_delay;
            p->in_interaction = true;
            p->change_behavior_to(selected_behavior);

            if(i.select_every_taget == false) { // Select random pony to interact with
                std::uniform_int_distribution<> dis(0, interaction_targets.size()-1);
                uint32_t rnd = dis(gen);

                interaction_targets[rnd]->current_interaction = i.name;
                interaction_targets[rnd]->current_interaction_delay = i.reactivation_delay;
                interaction_targets[rnd]->in_interaction = true;
                interaction_targets[rnd]->change_behavior_to(selected_behavior);
                qDebug() << p->name << "interacting with" << interaction_targets[rnd]->name << "using behavior" << selected_behavior << "for interaction" << i.name;
            }else{
                // Interact with all suitable ponies
                for(auto &t: interaction_targets) {
                    t->current_interaction = i.name;
                    t->current_interaction_delay = i.reactivation_delay;
                    t->in_interaction = true;
                    t->change_behavior_to(selected_behavior);
                    qDebug() << p->name << " interacting with " << t->name << " using " << selected_behavior << " for interaction " << i.name;
                }
            }
        }
    }
}

void ConfigWindow::receiveFromInstance(const QString &message) {
    if ( message == "showConfigWindow" )
        show();
}

void ConfigWindow::closeEvent(QCloseEvent *event)
{
    // To prevent silly warnings;
    event = event;
    if ( conf_read("main","hide-tray-icon").toBool() && active_list_model->rowCount() == 0 ) {
        qApp->quit();
    }
}

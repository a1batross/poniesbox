poniesbox
=========

poniesbox is an implementation of [DesktopPonies](http://desktopponies.bugs3.com/) in Qt.
All pony data (images, behaviors, sounds) is taken from DesktopPonies, and is compatible with it.

Inspired by [qponies](https://github.com/svenstaro/qponies) project by svenstaro,
forked from [qt-ponies](https://github.com/myszha/qt-ponies) by mysha.

Installation
------------
Compile the program or download a precompiled binary from [Downloads](https://bitbucket.org/XRevan86/poniesbox/downloads).

* Under Gentoo, you must:
```
layman -a alba-overlay
emerge poniesbox
```
* Under Arch Linux, use AUR: [poniesbox](https://aur.archlinux.org/packages/poniesbox);
* Under openSUSE, you must take a package from [here](http://software.opensuse.org/package/poniesbox);
* Under Debian-based, you must download and install poniesbox\_$version\_$arch.deb and poniesbox-data\_$version\_all.deb;
* Under Windows, download and unpack poniesbox-$version-win32.zip .

Running
-------
Run 'poniesbox'. If you have not added any ponies, the configuration window
will be shown (as is the case at first startup).

To open the configuration window, you can double-click on the system tray
icon or right-click it and choose 'Open configuration'. You can also close
the appliacation from this menu.

Right clicking on a pony brings up its context menu, where you can put that
pony to sleep, remove it, remove all instances of that pony, or open configuration window.

Compilation
-----------
A compiler supporting C\+\+11 (g++ >= 4.6, clang >= 3.1) is required, as are Qt (version >= 4.7) libraries (and X11 development libraries on X11 systems with Qt4).

Under Debian/Ubuntu you can install the dependencies by invoking:

    # apt-get install build-essential libqt5core5 libqt5gui5 qtbase5-dev qt5-qmake

or

    # apt-get install build-essential libx11-dev libxfixes-dev libqtcore4 libqtgui4 libqt5-dev qt4-qmake

Then build poniesbox by invoking:

    # git clone https://bitbucket.org/XRevan86/poniesbox.git
    # cd poniesbox
    # qmake
    # make

Feedback
--------
All requests like new DesktopPonies or interactions, feature requests, regression warnings, etc. goes to [Issues](https://bitbucket.org/XRevan86/poniesbox/issues) page,
any feedback is wanted :-).
	
Other information
-----------------
This is a work in progress.
Tested on GNU/Linux x86-64 with g++ 4.8, Qt 4.8/5.2, clang 3.2.

Due to case-sensitivity of filenames under UNIX, all 'pony.ini' files
must be lower case. The case of .gif files in pony.ini must also be
correct.

On UNIX systems an X server supporting ARGB visuals and and a compositing
window manager are required for transparency of pony windows. Something like
Compiz, GNOME Shell, Kwin, compton and most newer window managers support it.

**Configuration**

The configuration file is kept in:

On UNIX:

    $HOME/.config/poniesbox/poniesbox.ini

On Windows:

    %APPDATA%\poniesbox\poniesbox.ini


Screenshots of the configuration window
---------------------------------------

* Add pony screen:

![](http://storage6.static.itmages.com/i/13/1228/h_1388263323_5229887_7ffe82c495.png)


* Active ponies screen:

![](http://storage7.static.itmages.com/i/13/1228/h_1388263333_5254969_3f62159593.png)

* Configuration window screen:

![](http://storage9.static.itmages.com/i/13/1228/h_1388263960_7485955_d0b8ac5450.png)

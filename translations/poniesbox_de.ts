<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>ConfigWindow</name>
    <message>
        <location filename="../src/configwindow.ui" line="14"/>
        <source>Poniesbox</source>
        <translation>Poniesbox</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="63"/>
        <source>Add pony</source>
        <translation>Pony hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="220"/>
        <source>Remove pony</source>
        <translation>Pony entfernen</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="293"/>
        <source>&amp;Main</source>
        <translation>&amp;Haupt-</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="309"/>
        <source>Show the ponies in half size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="312"/>
        <source>&amp;Small ponies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="345"/>
        <source>Toggles if ponies are always on top of other windows</source>
        <translation>Einschalten um die Ponys immer im Vordergrund zu haben</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="348"/>
        <source>&amp;Always on top</source>
        <translation>Immer im Vordergrund (&amp;A)</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="472"/>
        <source>Hide &amp;tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="322"/>
        <source>Bypass the X11 window manager, showing ponies on every desktop on top of every window</source>
        <translation>Umgehe den X11 Windowsmanager, zeige die Ponys auf jedem Desktop</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="325"/>
        <source>Show on every &amp;virtual desktop</source>
        <translation>Zeig Ponys auf jedem &amp;virtuellem Desktop</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="388"/>
        <source>Specifies in which directory the pony data and configuration are</source>
        <translation>Gib an in welchem Ordner die Ponys sind</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="391"/>
        <source>Pony data di&amp;rectory</source>
        <translation>Pony O&amp;rdner</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="358"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="365"/>
        <source>Toggle if interactions are to be executed</source>
        <translation>Interaktionen Ein/Ausschalten</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="368"/>
        <source>&amp;Interactions enabled</source>
        <translation>&amp;Interaktionen aktivieren</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="411"/>
        <source>Toggle if effects are enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="414"/>
        <source>Ef&amp;fects enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="490"/>
        <source>S&amp;peech</source>
        <translation>S&amp;prache</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="508"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="537"/>
        <source>For how long the speech is to stay on screen</source>
        <translation>Wie lange soll der Text angezeigt werden</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="540"/>
        <source>&amp;Text delay</source>
        <translation>&amp;Textverzögerung</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="550"/>
        <source>Toggles if ponies are to speak</source>
        <translation>Srache Ein/Ausschalten</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="553"/>
        <source>&amp;Speech enabled</source>
        <translation>&amp;Sprache aktiviert</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="582"/>
        <source>NOT ACTIVE. Toggles the playing of sounds when a pony speaks.</source>
        <translation>NICHT AKTIV. Sound abspielen wenn ein Pony spricht.</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="585"/>
        <source>Play s&amp;ounds</source>
        <translation>Spiele S&amp;ound</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="605"/>
        <source>How frequently ponies speak</source>
        <translation>Wie häufig sprechen die Ponys</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="608"/>
        <source>Sp&amp;eech probability</source>
        <translation>Spr&amp;echwahrscheinlichkeit</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="621"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="262"/>
        <source>Accept</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="282"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="77"/>
        <source>Open configuration</source>
        <translation>Öffne Konfiguration</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="78"/>
        <source>Close application</source>
        <translation>Beende Poniesbox</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="29"/>
        <source>Add ponies</source>
        <translation>Ponys hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="205"/>
        <source>Active ponies</source>
        <translation>Aktive Ponys</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="256"/>
        <source>Configuration</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="425"/>
        <source>Select pony data directory</source>
        <translation>Wähle Ort der Ponydateien</translation>
    </message>
</context>
<context>
    <name>Pony</name>
    <message>
        <location filename="../src/pony.cpp" line="163"/>
        <source>Sleeping</source>
        <translation>Anhalten</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="170"/>
        <source>Remove %1</source>
        <translation>Entferne %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="171"/>
        <source>Remove every %1</source>
        <translation>Entferne alle %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="172"/>
        <source>Open configuration</source>
        <translation>Öffne Konfiguration</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="174"/>
        <source>Close application</source>
        <translation>Beende Poniesbox</translation>
    </message>
</context>
</TS>

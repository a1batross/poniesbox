<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="el_GR">
<context>
    <name>ConfigWindow</name>
    <message>
        <location filename="../src/configwindow.ui" line="14"/>
        <source>Poniesbox</source>
        <translation>Poniesbox</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="63"/>
        <source>Add pony</source>
        <translation>Πρόθεσε πόνι</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="220"/>
        <source>Remove pony</source>
        <translation>Αφαίρεσε πόνι</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="293"/>
        <source>&amp;Main</source>
        <translation>&amp;Γενίκα</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="309"/>
        <source>Show the ponies in half size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="312"/>
        <source>&amp;Small ponies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="345"/>
        <source>Toggles if ponies are always on top of other windows</source>
        <translation>Εναλλαγή αν τα πόνι είναι πάντα πάνω σε άλλα παράθυρα</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="348"/>
        <source>&amp;Always on top</source>
        <translation>Πάντ&amp;α από πάνω</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="472"/>
        <source>Hide &amp;tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="322"/>
        <source>Bypass the X11 window manager, showing ponies on every desktop on top of every window</source>
        <translation>Παράκαμψε το Χ11 διαχειριστή παραθύρων, δείχνωντας πόνι σε ολά τα επιφάνεια εργασίας από πάνω κάθε παράθυρο</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="325"/>
        <source>Show on every &amp;virtual desktop</source>
        <translation>Δείξε σε κάθε εικο&amp;νικό επιφάνεια εργασίας</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="388"/>
        <source>Specifies in which directory the pony data and configuration are</source>
        <translation>Προσδιορίζει σε ποίο κατάλογο τα δεδομένα του πόνι και διαμόρωση είναι</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="391"/>
        <source>Pony data di&amp;rectory</source>
        <translation>Κα&amp;τάλογος δεδομένων του πόνι</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="358"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="365"/>
        <source>Toggle if interactions are to be executed</source>
        <translation>Εναλλαγή αν αλληλεπιδράσεις πρέπει να εκτελεστούν</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="368"/>
        <source>&amp;Interactions enabled</source>
        <translation>&amp;Αλληλεπιδράσεις ενεργοποιμένα</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="411"/>
        <source>Toggle if effects are enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="414"/>
        <source>Ef&amp;fects enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="490"/>
        <source>S&amp;peech</source>
        <translation>Ο&amp;μιλία</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="508"/>
        <source> ms</source>
        <translation>χιλ./δευτ</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="537"/>
        <source>For how long the speech is to stay on screen</source>
        <translation>Για πόσο καιρό η ομιλία θα μείνει στην οθόνη</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="540"/>
        <source>&amp;Text delay</source>
        <translation>&amp;Καθυστέρηση ομιλίας</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="550"/>
        <source>Toggles if ponies are to speak</source>
        <translation>Εναλλαγη αν τα πόνι θα μιλούν</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="553"/>
        <source>&amp;Speech enabled</source>
        <translation>&amp;Ενεροποίση ομιλίας</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="582"/>
        <source>NOT ACTIVE. Toggles the playing of sounds when a pony speaks.</source>
        <translation>ΌΧΙ ΕΡΓΟΝΟΠΟΙΜΈΝΑ. Εναλλαγή η αναπαραγωγή ήχου όταν μιλαέι ένα πόνι.</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="585"/>
        <source>Play s&amp;ounds</source>
        <translation>Παίζει η&amp;χους</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="605"/>
        <source>How frequently ponies speak</source>
        <translation>Ποσό συχνά μιλάνε τα πόνι</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="608"/>
        <source>Sp&amp;eech probability</source>
        <translation>Πιθανότητες oμ&amp;ιλίας</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="621"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="262"/>
        <source>Accept</source>
        <translation>Αποδοχή</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="282"/>
        <source>Reset</source>
        <translation>Επαναφορά</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="77"/>
        <source>Open configuration</source>
        <translation>Ανοιξέ διαμόρφωση</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="78"/>
        <source>Close application</source>
        <translation>Κλείσε εμφαρμογή</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="29"/>
        <source>Add ponies</source>
        <translation>Προσθέσε πόνι</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="205"/>
        <source>Active ponies</source>
        <translation>Ενεργά πόνι</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="256"/>
        <source>Configuration</source>
        <translation>Διαμορφώση</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="425"/>
        <source>Select pony data directory</source>
        <translation>Διάλογη κατάλογο δεδομένων πόνι</translation>
    </message>
</context>
<context>
    <name>Pony</name>
    <message>
        <location filename="../src/pony.cpp" line="163"/>
        <source>Sleeping</source>
        <translation>Κοιμούνται</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="170"/>
        <source>Remove %1</source>
        <translation>Αφάιρεση %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="171"/>
        <source>Remove every %1</source>
        <translation>Αφάιρεση όλων %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="172"/>
        <source>Open configuration</source>
        <translation>Ανοιξέ διαμόρφωση</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="174"/>
        <source>Close application</source>
        <translation>Κλείσε εμφαρμογή</translation>
    </message>
</context>
</TS>
